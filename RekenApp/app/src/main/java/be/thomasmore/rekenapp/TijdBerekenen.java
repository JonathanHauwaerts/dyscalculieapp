package be.thomasmore.rekenapp;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import org.joda.time.LocalDateTime;

import java.util.List;

public class TijdBerekenen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";


    LocalDateTime beginTijd = null;
    LocalDateTime eindTijd = null;


    private EditText tijdPickerBegin;
    private EditText tijdPickerEind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tijd_berekenen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tijdPickerBegin = (EditText) findViewById(R.id.txt_tijd_verschil_beginuur);
        tijdPickerEind = (EditText) findViewById(R.id.txt_tijd_verschil_einduur);

        tijdPickerBegin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int uur,minuut,seconden;
                uur= 1;
                minuut= 1;


                if (useDarkTheme){
                    TimePickerDialog beginTimePickerDialog = new TimePickerDialog(v.getContext(), android.R.style.Theme_Holo_Dialog, new TimePickerDialog.OnTimeSetListener(){
                        @Override
                        public void onTimeSet(TimePicker t,int uur, int minuut){
                            tijdPickerBegin.setText(uur + ":" +minuut);
                            if (minuut < 10){
                                tijdPickerBegin.setText(uur + ":0" +minuut);
                            }
                            beginTijd = new LocalDateTime(2017,12,12,uur,minuut);
                        }
                    },uur,minuut,true);

                    beginTimePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    beginTimePickerDialog.show();
                }else {
                    TimePickerDialog beginTimePickerDialog = new TimePickerDialog(v.getContext(), android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener(){
                        @Override
                        public void onTimeSet(TimePicker t,int uur, int minuut){
                            tijdPickerBegin.setText(uur + ":" +minuut);
                            if (minuut < 10){
                                tijdPickerBegin.setText(uur + ":0" +minuut);
                            }
                            beginTijd = new LocalDateTime(2017,12,12,uur,minuut);
                        }
                    },uur,minuut,true);

                    beginTimePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    beginTimePickerDialog.show();
                }

            }
        });

        tijdPickerEind.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int uur,minuut,seconden;
                uur= 1;
                minuut= 1;


                if (useDarkTheme){
                    TimePickerDialog eindTimePickerDialog = new TimePickerDialog(v.getContext(), android.R.style.Theme_Holo_Dialog, new TimePickerDialog.OnTimeSetListener(){
                        @Override
                        public void onTimeSet(TimePicker t,int uur, int minuut){
                            tijdPickerEind.setText(uur + ":" +minuut);
                            if (minuut < 10){
                                tijdPickerEind.setText(uur + ":0" +minuut);
                            }
                            eindTijd = new LocalDateTime(2017,12,12,uur,minuut);
                        }
                    },uur,minuut,true);

                    eindTimePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    eindTimePickerDialog.show();
                }else {
                    TimePickerDialog eindTimePickerDialog = new TimePickerDialog(v.getContext(), android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener(){
                        @Override
                        public void onTimeSet(TimePicker t,int uur, int minuut){
                            tijdPickerEind.setText(uur + ":" +minuut);
                            if (minuut < 10){
                                tijdPickerEind.setText(uur + ":0" +minuut);
                            }
                            eindTijd = new LocalDateTime(2017,12,12,uur,minuut);
                        }
                    },uur,minuut,true);

                    eindTimePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    eindTimePickerDialog.show();
                }


            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onClickButtonTijdBerekenen(View V){

        int beginuur = beginTijd.getHourOfDay();
        int einduur = eindTijd.getHourOfDay();
        int eindminuten = eindTijd.getMinuteOfHour();

        if (einduur < beginuur  ){
            eindTijd = new LocalDateTime(2017,12,13,einduur,eindminuten);
        }

        berekenTijd();

    }



    private void berekenTijd(){


        EditText beginUurText = (EditText)findViewById(R.id.txt_tijd_verschil_beginuur);
      EditText eindUurText = (EditText)findViewById(R.id.txt_tijd_verschil_einduur);

        if (!(beginUurText.getText().toString().matches(""))&&!(eindUurText.getText().toString().matches(""))){
            List resultaat = Berekeningen.TijdBerekenen(beginTijd,eindTijd);

            EditText outputminuut = (EditText)findViewById(R.id.txt_tijd_verschil_uiteindelijkminuut) ;
            EditText outputuur = (EditText)findViewById(R.id.txt_tijd_verschil_uiteindelijkuur);
            outputuur.setText(String.valueOf(resultaat.get(0)));
            outputminuut.setText(String.valueOf(resultaat.get(1)));


        }else {
            Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
        }
    }

}
