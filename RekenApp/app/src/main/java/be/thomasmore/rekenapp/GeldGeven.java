package be.thomasmore.rekenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GeldGeven extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    List<String> berekeningen= new ArrayList<>();

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    //quickfill
    EditText budget;
    EditText tebetalen;
    Button berekenen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geld_geven);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        berekeningen= Berekeningen.haalRekengeschiedenisOp(getApplicationContext());


        //quickfill
        budget = (EditText)findViewById(R.id.txt_geldgeven_budget);
        tebetalen = (EditText)findViewById(R.id.txt_geldgeven_tegevenbedrag);
        berekenen = (Button)findViewById(R.id.btn_geldgeven_berekenen);

        budget.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    tebetalen.requestFocus();
                }
                return false;
            }
        });

        tebetalen.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    berekenen.requestFocus();
                    bereken();
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //ALTERNATIEVE MANIER OM TE WERKEN :
    public void onClickButtonGeldgeven(View v){
        bereken();
    }

    private void bereken(){
        if (!(String.valueOf(budget.getText().toString()).matches(""))&&!(String.valueOf(tebetalen.getText().toString()).matches(""))){
            verwerken(budget,tebetalen);
        }else {
            Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
        }
    }

    private void verwerken(EditText budgettxt, EditText tebetalentxt){
        double budget = Double.parseDouble(budgettxt.getText().toString());
        double tebetalen = Double.parseDouble(tebetalentxt.getText().toString());

        double resultaat = Berekeningen.geldgeven(budget,tebetalen);
        berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Geld geven: budget - tebetalen bedrag : "+budget+ " - " +tebetalen+"= " + resultaat,getApplicationContext());
        giveOutput(resultaat);
    }

    private void giveOutput(double result){
        EditText output = (EditText)findViewById(R.id.txt_geldgeven_huidig_budget);
        output.setText(String.valueOf(result));
        if (result < 0){
            Toast.makeText(getBaseContext(), "Waarschuwing  : uw budget gaat onder 0!", Toast.LENGTH_SHORT).show();
        }
    }
}
