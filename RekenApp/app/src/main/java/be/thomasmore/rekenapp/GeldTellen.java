package be.thomasmore.rekenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class GeldTellen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    List<String> filenames;

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geld_tellen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        leesLogos();
        uibuild();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void leesLogos()
    {
        filenames = new ArrayList<String>();
        Field[] drawables = be.thomasmore.rekenapp.R.drawable.class.getFields();
        for (Field f : drawables) {
            if (f.getName().startsWith("munt_")) {
                filenames.add(f.getName());
            }
        }
    }

    public void uibuild(){
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.munten);

        for (int i = 1; i <= 15; i++) {
            LinearLayout linearLayoutNew = new LinearLayout(this);
            linearLayoutNew.setId(1000+i);
            linearLayoutNew.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams layoutTextParams =
                    new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
            linearLayoutNew.setLayoutParams(layoutTextParams);

            ImageView imageView = new ImageView(this);
            LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(100,100);
            imageLayoutParams.leftMargin = 5;
            imageLayoutParams.topMargin = 5;
            imageView.setLayoutParams(imageLayoutParams);
            imageView.setTag(filenames.get(i-1));

            imageView.setImageResource(getResources().getIdentifier(filenames.get(i-1), "drawable", getPackageName()));


            linearLayoutNew.addView(imageView);

            Button knp1 = new Button(this);
            knp1.setId(50+i);
            knp1.setText("-");
            knp1.setTextSize(15);
            knp1.setPadding(1,1,1,1);
            knp1.setWidth(15);
            knp1.setHeight(15);

            knp1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    geldAfhalen(v);
                }
            });
            linearLayoutNew.addView(knp1);

            TextView textviewAantal = new TextView(this);
            textviewAantal.setId(200+i);
            textviewAantal.setWidth(100);
            textviewAantal.setHeight(50);
            textviewAantal.setText("0");
            linearLayoutNew.addView(textviewAantal);

            Button knp = new Button(this);
            knp.setId(i);
            knp.setText("+");
            knp.setTextSize(15);
            knp.setWidth(15);
            knp.setHeight(15);
            knp.setPadding(1,1,1,1);

            knp.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    geldToevoegen(v);
                }
            });
            linearLayoutNew.addView(knp);

            linearLayout.addView(linearLayoutNew);
        }
    }
    public void Reset(View v){
        TextView prijs = (TextView)findViewById(R.id.TotaalUitkomst);
        prijs.setText("0");

        for (int i = 1; i <= 15; i++) {
            int id = 200 +i;
            TextView aantal = (TextView)findViewById(id);
            aantal.setText("0");
        }
    }
    public void geldToevoegen (View v){

        Button b = (Button)v;
        int buttonText = b.getId();

        TextView prijs = (TextView)findViewById(R.id.TotaalUitkomst);
        double uitkomst =Double.parseDouble(prijs.getText().toString());

        int id = buttonText;
        id += 200;

        TextView aantal = (TextView)findViewById(id);
        int aantalMunten =Integer.parseInt(aantal.getText().toString());

        switch (buttonText){
            case 1:
                uitkomst +=0.01;
                break;
            case 2:
                uitkomst +=0.02;
                break;
            case 3:
                uitkomst +=0.05;
                break;
            case 4:
                uitkomst +=0.10;
                break;
            case 5:
                uitkomst +=0.20;
                break;
            case 6:
                uitkomst +=0.50;
                break;
            case 7:
                uitkomst +=1.00;
                break;
            case 8:
                uitkomst +=2.00;
                break;
            case 9:
                uitkomst +=5.00;
                break;
            case 10:
                uitkomst +=10.00;
                break;
            case 11:
                uitkomst +=20.00;
                break;
            case 12:
                uitkomst +=50.00;
                break;
            case 13:
                uitkomst +=100.00;
                break;
            case 14:
                uitkomst +=200.00;
                break;
            case 15:
                uitkomst +=500.00;
                break;
        }
        aantalMunten += 1;
        aantal.setText(Integer.toString(aantalMunten));

        uitkomst = uitkomst *100;
        uitkomst = Math.round(uitkomst);
        uitkomst = uitkomst/100;

        prijs.setText(String.valueOf(uitkomst));
    }

    public void geldAfhalen (View v){

        Button b = (Button)v;
        int buttonText = b.getId();

        TextView prijs = (TextView)findViewById(R.id.TotaalUitkomst);
        double uitkomst =Double.parseDouble(prijs.getText().toString());


        int id = buttonText;
        id += 150;

        TextView aantal = (TextView)findViewById(id);
        int aantalMunten =Integer.parseInt(aantal.getText().toString());

        if (aantalMunten > 0 ){
            switch (buttonText){
                case 51:
                    uitkomst -=0.01;
                    break;
                case 52:
                    uitkomst -=0.02;
                    break;
                case 53:
                    uitkomst -=0.05;
                    break;
                case 54:
                    uitkomst -=0.10;
                    break;
                case 55:
                    uitkomst -=0.20;
                    break;
                case 56:
                    uitkomst -=0.50;
                    break;
                case 57:
                    uitkomst -=1.00;
                    break;
                case 58:
                    uitkomst -=2.00;
                    break;
                case 59:
                    uitkomst -=5.00;
                    break;
                case 60:
                    uitkomst -=10.00;
                    break;
                case 61:
                    uitkomst -=20.00;
                    break;
                case 62:
                    uitkomst -=50.00;
                    break;
                case 63:
                    uitkomst -=100.00;
                    break;
                case 64:
                    uitkomst -=200.00;
                    break;
                case 65:
                    uitkomst -=500.00;
                    break;
            }
            aantalMunten -= 1;
            aantal.setText(String.valueOf(aantalMunten));

            uitkomst = uitkomst *100;
            uitkomst = Math.round(uitkomst);
            uitkomst = uitkomst/100;

            prijs.setText(String.valueOf(uitkomst));
        }else {
            Toast.makeText(getBaseContext(), "U hebt deze munt niet", Toast.LENGTH_SHORT).show();

        }

    }

}
