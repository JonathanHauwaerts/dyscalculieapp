package be.thomasmore.rekenapp;

import android.content.Context;
import android.util.Log;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Gebruiker on 03/12/2017.
 */
//TODO fix rounding issues

public class Berekeningen{
    public static double soldenBerekenen(double huidigeprijs, double solden,Context context,List<String> lijst){
        Log.i("dysk", "in classe berekenen");
        double totaleprijs1,totaleprijs2;

        totaleprijs1 = (huidigeprijs / 100)*solden;
        totaleprijs2 = huidigeprijs - totaleprijs1;
        vulRekengeschiedenis(lijst,"Korting: (huidigeprijs/100) X kortingpercentage : (" + huidigeprijs +"/100) X " + solden +"=" +totaleprijs1,context);
        Log.i("dysk", "berekening uitgevoerdt");
        Log.i("dysk", "going to return value");
        return totaleprijs2;

    }


    public static double berekenTijdAfstand_afstand(double snelheidinKm,double tijduur,double tijdminuut){
        double resultaat =0.0;
        //TODO berekening uitdokteren
        double afstand = snelheidinKm * (tijduur + (tijdminuut/60));
        resultaat = afstand;
        return resultaat;
    }

    public static double berekenTijdAfstand_snelheid(double afstand,double tijduur,double tijdminuut){
        double resultaat = 0.0;
        //TODO berekening uitdokteren
        double snelheid = afstand / (tijduur + (tijdminuut/60));
        resultaat = snelheid;
        return resultaat;
    }

    public static List berekenTijdAfstand_tijd(double afstand, double snelheid){
        List resultaat = new ArrayList();
        double tijdinUur = afstand / snelheid;
        double tijdinMinuten = tijdinUur * 60;
        int uren = 0;
        double minuten = tijdinMinuten;


        while (minuten>=60){
            minuten = minuten- 60;
            uren = uren +1;
        }



        resultaat.add(uren);
        resultaat.add(minuten);
        return  resultaat;
    }

    public static double berekenWisselgeld(double tebetalenbedrag, double gegevenbedrag){
        double resultaat;
        resultaat= gegevenbedrag - tebetalenbedrag;
        return resultaat;
    }

    public static double geldgeven(double budget,double tebetalen){


        return  budget - tebetalen;
    }

    public static List TijdBerekenen(LocalDateTime beginUur, LocalDateTime eindUur ){
        Period verschilTijd= new Period(beginUur,eindUur);
        List resultaat = new ArrayList();

        resultaat.add(String.valueOf(verschilTijd.getHours()));
        resultaat.add(String.valueOf(verschilTijd.getMinutes()));
        return resultaat;
    }

    public static List datumBerekenen(LocalDate beginDatum, LocalDate eindDatum){
        List verschillen = new ArrayList();

        Period intervalPeriod = new Period( beginDatum, eindDatum, PeriodType.yearMonthDay());

        long days = intervalPeriod.getDays();
        long months = intervalPeriod.getMonths();
        long years = intervalPeriod.getYears();

        verschillen.add(days);
        verschillen.add(months);
        verschillen.add(years);

        return verschillen;
    }

    public static LocalDate dagenRekenen(boolean moetOptellen, LocalDate beginDatum, int aantalDagen){
        LocalDate rekendatum = null;

        if (moetOptellen == true){
           rekendatum = beginDatum.plusDays(aantalDagen);
        }else if(moetOptellen == false){
         rekendatum =   beginDatum.minusDays(aantalDagen);
        }

        return rekendatum;
    }

    public static List omzettenGewicht(double gewichtInKilogram){
        List gewicht = new ArrayList();

        gewicht.add(gewichtInKilogram * 1000000);
        gewicht.add(gewichtInKilogram * 1000);
        gewicht.add(gewichtInKilogram);
        gewicht.add(gewichtInKilogram / 1000);

        return gewicht;
    }

    public static List omzettenVolume(double volumeInCl){
        List volume = new ArrayList();
        //ml
        volume.add(volumeInCl * 10);
        //cl
        volume.add(volumeInCl);
        //dl
        volume.add(volumeInCl / 10);
        //l
        volume.add(volumeInCl / 100);

        return volume;
    }

    public static List aftstandOmzetten(double afstandInMeter){
        List afstand = new ArrayList();

        afstand.add(afstandInMeter * 1000);
        afstand.add(afstandInMeter * 100);
        afstand.add(afstandInMeter *10);
        afstand.add(afstandInMeter);
        afstand.add(afstandInMeter /1000);

        return afstand;
    }
    public static List<Double> oppervlakteConverter(double oppervlakteInMeter){
        List<Double> oppervlakte = new ArrayList<Double>();

        /* meters */
        oppervlakte.add(oppervlakteInMeter * 10000);
        oppervlakte.add(oppervlakteInMeter *100 );
        oppervlakte.add(oppervlakteInMeter);
        oppervlakte.add(oppervlakteInMeter /1000000);

        /* are */
        oppervlakte.add(oppervlakteInMeter / 100);
        oppervlakte.add(oppervlakteInMeter / 10000);

        return oppervlakte;
    }

    public static Double oppervlakteRechthoek(double breedteInMeter,double lengteInMeter){

        double oppervlakteInMeter;

        oppervlakteInMeter = breedteInMeter * lengteInMeter;

        return oppervlakteInMeter;
    }

    public static Double oppervlakteDriehoek(double breedteInMeter, double hoogteInMeter){

        double oppervlakteInMeter;

        oppervlakteInMeter = (breedteInMeter * hoogteInMeter)/2;



        return oppervlakteInMeter;
    }

    public static Double oppervlakteCirkel(double straal){

        double oppervlakteInMeter;

        oppervlakteInMeter = Math.PI*(straal * straal);



        return  oppervlakteInMeter;
    }

    public static Double oppervlakteTrapezium(double grotebasis, double kleinebasis, double hoogte){

        double oppervlakteInMeter;

        oppervlakteInMeter = ((grotebasis+ kleinebasis)*hoogte)/2;



        return  oppervlakteInMeter;
    }

    public static Double oppervlakteRuit(double groteDiagonale, double kleineDiagonale){

        double oppervlakteInMeter;

        oppervlakteInMeter = (groteDiagonale*kleineDiagonale)/2;

        return oppervlakteInMeter;
    }

    public static Double oppervlakteParallellogram(double basis, double hoogte){

        double oppervlakteInMeter;

        oppervlakteInMeter = basis * hoogte;

        return oppervlakteInMeter;
    }
    public static List<String> vulRekengeschiedenis(List<String> berekeningen,String formule,Context context){
        String file_name;
       if (berekeningen.size() >= 10){
           berekeningen.remove(0);
       }
       berekeningen.add(formule);


        try {
            FileOutputStream fileOutputStream = context.openFileOutput("RekenGeschiedenis",MODE_PRIVATE);

            for (int i =0; i<=berekeningen.size();i++){
                fileOutputStream.write((berekeningen.get(i)+"§").getBytes());

            }
            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return  berekeningen;
    }

    public static List<String> haalRekengeschiedenisOp(Context context) {
        List<String> berekeningenLijst = new ArrayList<>();
        try {
            String message;
            FileInputStream fileInputStream = context.openFileInput("RekenGeschiedenis");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            int read = -1;
            StringBuffer buffer = new StringBuffer();
            String formules="";

            while((read =bufferedReader.read())!= -1){
                formules = formules + (char)read;
            }


            for(String formule : formules.split("§")){
                berekeningenLijst.add(formule);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.reverse(berekeningenLijst);
        return berekeningenLijst;
    }

    public static void wisgeschiedenis(Context context){
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("RekenGeschiedenis",MODE_PRIVATE);


                fileOutputStream.write(("").getBytes());
                fileOutputStream.flush();

            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
