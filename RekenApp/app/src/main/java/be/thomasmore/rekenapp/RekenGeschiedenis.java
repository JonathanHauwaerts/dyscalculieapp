package be.thomasmore.rekenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RekenGeschiedenis extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    List<String> berekeningenLijst = new ArrayList<>();

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reken_geschiedenis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        berekeningenLijst = Berekeningen.haalRekengeschiedenisOp(this.getApplicationContext());

        //fillUi();
        useCustomAdapter();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void useCustomAdapter()
    {
        RekengeschiedenisAdapter Rekadapter = new RekengeschiedenisAdapter(getApplicationContext(), berekeningenLijst);

        final ListView listViewPlatforms =
                (ListView) findViewById(R.id.listviewGeschiedenis);
        listViewPlatforms.setAdapter(Rekadapter);

    }

    private void fillUi(){
        berekeningenLijst = Berekeningen.haalRekengeschiedenisOp(getApplicationContext());

        //LinearLayout linearLayout = (LinearLayout) findViewById(R.id.berekeningen);

        for(int i = 0; i <= berekeningenLijst.size()-1; i++){
            LinearLayout linearLayoutNew = new LinearLayout(this);
            linearLayoutNew.setId(1000+i);
            linearLayoutNew.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams layoutLinParams =
                    new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutLinParams.setMargins(0,5,0,15);
            linearLayoutNew.setLayoutParams(layoutLinParams);
            linearLayoutNew.setBackgroundResource(R.drawable.border_black);


            TextView textview = new TextView(this);
            textview.setId(i);
            LinearLayout.LayoutParams layoutTextParams =
                    new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutTextParams.setMargins(0,0,0,0);
            textview.setLayoutParams(layoutTextParams);
            textview.setBackgroundResource(R.drawable.heading);
            textview.setText(berekeningenLijst.get(i).split(":")[0]);
            textview.setTextSize(25);
            linearLayoutNew.addView(textview);

            TextView textview2 = new TextView(this);
            textview2.setId(100+i);
            LinearLayout.LayoutParams layoutText2Params =
                    new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutTextParams.setMargins(5,5,5,5);
            textview2.setLayoutParams(layoutText2Params);
            textview2.setText(berekeningenLijst.get(i).split(":")[1]);
            textview.setTextColor(Color.parseColor("black"));
            textview2.setTextSize(15);
            linearLayoutNew.addView(textview2);
            String ber[] = berekeningenLijst.get(i).split(":");
            if(ber.length >2 ){
                TextView textview3 = new TextView(this);
                textview3.setId(100+i);
                LinearLayout.LayoutParams layoutText3Params =
                        new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutText3Params.setMargins(5,5,5,5);
                textview3.setLayoutParams(layoutText3Params);
                textview3.setText(berekeningenLijst.get(i).split(":")[2]);
                textview.setTextColor(Color.parseColor("black"));
                textview3.setTextSize(15);
                linearLayoutNew.addView(textview3);
            }

            //linearLayout.addView(linearLayoutNew);
        }
    }
}
