package be.thomasmore.rekenapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.List;


public class DatumBerekenen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    int beginday;
    int beginyear;
    int beginmonth;

    LocalDate beginDatum = null;
    LocalDate eindDatum = null;


    private EditText datePickerBegin;
    private EditText datePickerEind;

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);

        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datum_berekenen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //setupSpinners();

        datePickerEind = (EditText) findViewById(R.id.txt_datum_verschil_input_einddatum);
        datePickerBegin = (EditText) findViewById(R.id.txt_datum_verschil_input_begindatum);


        datePickerBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int dayOfMonth,month,year;
                year = DateTime.now().getYear();
                month = DateTime.now().getMonthOfYear();
                dayOfMonth = DateTime.now().getDayOfMonth();

                if (useDarkTheme){
                    DatePickerDialog begindateDialog = new DatePickerDialog(v.getContext(), android.R.style.Theme_Holo_Dialog, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month +=1;
                            datePickerBegin.setText(dayOfMonth+"/"+month+"/"+year);
                            beginDatum = new LocalDate(year,month,dayOfMonth);
                        }
                    },year,month,dayOfMonth);
                    begindateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    begindateDialog.show();
                }else {
                    DatePickerDialog begindateDialog = new DatePickerDialog(v.getContext(), android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month +=1;
                            datePickerBegin.setText(dayOfMonth+"/"+month+"/"+year);
                            beginDatum = new LocalDate(year,month,dayOfMonth);
                        }
                    },year,month,dayOfMonth);
                    begindateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    begindateDialog.show();
                }


            }
        });

        datePickerEind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int dayOfMonth,month,year;
                year = DateTime.now().getYear();
                month = DateTime.now().getMonthOfYear();
                dayOfMonth = DateTime.now().getDayOfMonth();

                if (useDarkTheme){
                    DatePickerDialog einddateDialog = new DatePickerDialog(v.getContext(), android.R.style.Theme_Holo_Dialog, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month +=1;
                            datePickerEind.setText(dayOfMonth+"/"+month+"/"+year);
                            eindDatum = new LocalDate(year,month,dayOfMonth);

                        }
                    },year,month,dayOfMonth);
                    einddateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    einddateDialog.show();
                }else {
                    DatePickerDialog einddateDialog = new DatePickerDialog(v.getContext(), android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month +=1;
                            datePickerEind.setText(dayOfMonth+"/"+month+"/"+year);
                            eindDatum = new LocalDate(year,month,dayOfMonth);

                        }
                    },year,month,dayOfMonth);
                    einddateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    einddateDialog.show();
                }

            }
        });

    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }






    public void onClickButtonBerekenDatumVerschil(View v){
        berekenVerschilTussenDatums();
    }

    private void berekenVerschilTussenDatums(){
        if (!(eindDatum==null) && !(beginDatum==null)){
           List verschil = Berekeningen.datumBerekenen(beginDatum,eindDatum);

           EditText outputDagentext = (EditText)findViewById(R.id.txt_datum_verschil_output_dagen);
           EditText outputMaandentext = (EditText)findViewById(R.id.txt_datum_verschil_output_maanden);
           EditText outputJarentext = (EditText)findViewById(R.id.txt_datum_verschil_output_jaren);

           outputDagentext.setText(verschil.get(0).toString());
           outputMaandentext.setText(verschil.get(1).toString());
           outputJarentext.setText(verschil.get(2).toString());

        }else {
                Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
            }


    }


}
