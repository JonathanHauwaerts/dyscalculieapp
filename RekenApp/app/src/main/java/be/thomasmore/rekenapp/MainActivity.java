package be.thomasmore.rekenapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";


    public static Activity mainactiviteit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainactiviteit = this;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent= new Intent(this, Instellingen.class);
            intent.putExtra("mainActivity",true);
            startActivity(intent);

            return true;

        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);

            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);

            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);

            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);

            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);

            return true;
        } else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);

            return true;

        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);

            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);

            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);

            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);

            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);

            return true;
        } else if (id == R.id.nav_geldgeven) {
            Intent intent = new Intent(this, GeldGeven.class);
            startActivity(intent);

            return true;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //ALL the onclickbuttons

    public void onClickNavButtonAbout(View v){
        Intent intent= new Intent(this, About.class);
        startActivity(intent);
    }
    public void onClickNavButtonInstellingen(View v){
        Intent intent= new Intent(this, Instellingen.class);
        intent.putExtra("mainActivity",true);
        startActivity(intent);

    }

    public void onClickNavButtonRekengeschiedenis(View v){
        Intent intent= new Intent(this, RekenGeschiedenis.class);
        startActivity(intent);
    }

    public void onClickNavButtonVolumeomzetten(View v){
        Intent intent= new Intent(this, VolumeOmzetten.class);
        startActivity(intent);
    }

    public void onClickNavButtonOppervlakteberekenen(View v){
        Intent intent= new Intent(this, OppervlakteBerekenen.class);
        startActivity(intent);
    }

    public void onClickNavButtonAfstandenomzetten(View v){
        Intent intent= new Intent(this, AfstandenOmzetten.class);
        startActivity(intent);
    }

    public void onClickNavButtonGewichtomzetten(View v){
        Intent intent= new Intent(this, GewichtOmzetten.class);
        startActivity(intent);
    }

    public void onClickNavButtonTijdberekenen(View v){
        Intent intent= new Intent(this, TijdBerekenen.class);
        startActivity(intent);
    }

    public void onClickNavButtonTijdafstand(View v){
        Intent intent= new Intent(this, TijdAfstand.class);
        startActivity(intent);
    }

    public void onClickNavButtonDagenrekenen(View v){
        Intent intent= new Intent(this, DatumDagenRekenen.class);
        startActivity(intent);
    }

    public void onClickNavButtonDatumberekenen(View v){
        Intent intent= new Intent(this, DatumBerekenen.class);
        startActivity(intent);
    }

    public void onClickNavButtonSprekendeklok(View v){
        Intent intent= new Intent(this, SprekendeKlok.class);
        startActivity(intent);
    }

    public void onClickNavButtonGeldgeven(View v){
        Intent intent = new Intent(this, GeldGeven.class);
        startActivity(intent);
    }

    public void onClickNavButtonGeldtellen(View v){
        Intent intent= new Intent(this, GeldTellen.class);
        startActivity(intent);
    }

    public void onClickNavButtonWisselgeld(View v){
        Intent intent= new Intent(this, WisselgeldBerekenen.class);
        startActivity(intent);
    }

    public void onClickNavButtonSolden(View v){
        Intent intent= new Intent(this, SoldenBerekenen.class);
        startActivity(intent);
    }

}
