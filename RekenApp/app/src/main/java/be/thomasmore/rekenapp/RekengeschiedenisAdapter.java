package be.thomasmore.rekenapp;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class RekengeschiedenisAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> values;

    public RekengeschiedenisAdapter(Context context, List<String> values) {
        super(context, R.layout.geschiedenislistview, values);
        this.context = context;
        this.values = values;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.geschiedenislistview, parent, false);

        final TextView textViewTitel = (TextView) rowView.findViewById(R.id.titel);
        final TextView textViewFormule= (TextView) rowView.findViewById(R.id.formule);
        final TextView textViewBerekening = (TextView) rowView.findViewById(R.id.berekening);

        textViewTitel.setText(values.get(position).split(":")[0]);
        textViewFormule.setText(values.get(position).split(":")[1]);
        String ber[] = values.get(position).split(":");

        if(ber.length >2 ) {
            textViewBerekening.setText(values.get(position).split(":")[2]);
        }

        return rowView;
    }

}
