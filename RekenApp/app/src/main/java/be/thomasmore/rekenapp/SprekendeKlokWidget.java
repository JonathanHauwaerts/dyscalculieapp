package be.thomasmore.rekenapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.widget.AnalogClock;
import android.widget.DigitalClock;
import android.widget.RemoteViews;
import android.widget.TimePicker;

import org.joda.time.LocalDateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Implementation of App Widget functionality.
 */
public class SprekendeKlokWidget extends AppWidgetProvider {
    public static String YOUR_AWESOME_ACTION = "Leesklok";

    AnalogClock analogeklok;
    DigitalClock digitaleklok;
    TimePicker tijdpicker;
    TextToSpeech spreker;
    LocalDateTime tijdpickerUur;
    DateFormat df = new SimpleDateFormat("hh:mm:ss");

    protected void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.sprekende_klok_widget);

        // Instruct the widget manager to update the widget
        //appWidgetManager.updateAppWidget(appWidgetId, views);

        //onlclick van de widget
        Intent intent = new Intent(context,SprekendeKlok.class);
        intent.setAction(YOUR_AWESOME_ACTION);

        views.setTextViewText(R.id.digitalClock_sprekende_klokwidget,df.format(new Date()));

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        views.setOnClickPendingIntent(R.id.analogClock_sprekende_klokwidget,pendingIntent);
        views.setOnClickPendingIntent(R.id.digitalClock_sprekende_klokwidget,pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(YOUR_AWESOME_ACTION)) {
            LocalDateTime nu = new LocalDateTime().now();

            zegTijd(nu);
        }

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }


    private void zegTijd(LocalDateTime nu){
        String leesscript;
        leesscript = "Het is " + String.valueOf(nu.getHourOfDay()) + " uur en " + String.valueOf(nu.getMinuteOfHour())+ " minuten";
        //  TODO OPTIONEEL ALS ER TIJD OVER IS
        int minuten = nu.getMinuteOfHour();
        int uur = nu.getHourOfDay();
        boolean isLocked = false;

        if (minuten == 50){
            isLocked = true;
            //het is half uur+1
            if (uur <=12){
                uur = uur+1;
                leesscript ="Het is tien voor " + uur;
            }else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is tien voor " + uur;
                }
            }
        }
        if (minuten == 10){
            isLocked = true;
            //het is half uur+1
            if (uur <=12){

                leesscript ="Het is tien na " + uur;
            }else{
                uur = (uur-12);
                uur = Math.abs(uur);

                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is tien na " + uur;
                }
            }
        }
        if (minuten == 30){
            isLocked = true;
            //het is half uur+1
            if (uur <=12){
                uur = uur+1;
                leesscript ="Het is half " + uur;
            }else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is half " + uur;
                }
            }
        }
        if (minuten == 15){
            isLocked = true;
            if (uur <=12){

                leesscript ="Het is kwart over " + uur;
            }else{
                uur = (uur-12);
                uur = Math.abs(uur);

                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is kwart over " + uur;
                }
            }
        }
        if (minuten==45){
            isLocked = true;
            //het is kwart voor uur+1
            if (uur <=12){
                uur = uur+1;
                leesscript ="Het is kwart voor " + uur;
            }else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is kwart voor " + uur;
                }
            }
        }
        if (nu.getMinuteOfHour()==0){
            //het is uur
            //het is half uur+1
            isLocked = true;
            if (uur <=12){
                uur = uur+1;
                leesscript ="Het is " + uur + " uur";
                isLocked = true;
            }else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is  " + uur + " uur";
                }
                isLocked = true;
            }
        }else {
            //het is half uur+1
            if (isLocked == false){
                if (uur <=12){

                    leesscript ="Het is  " + uur + " uur " + minuten ;
                    if (minuten >=30){
                        uur = uur+1;
                        minuten = 60 - minuten;
                        leesscript = "Het is " + minuten + " voor " +  uur ;
                    }

                }else{
                    uur = (uur-12);
                    uur = Math.abs(uur);

                    if (uur >= 0){
                        if (uur == 0){
                            uur = 12;
                        }
                        leesscript = "Het is  " + uur + " uur " + minuten;
                        if (minuten >=30){
                            uur = uur+1;
                            minuten = 60 - minuten;
                            leesscript = "Het is " + minuten + " voor " +  uur;
                        }
                    }
                }
            }

        }


        spreker.speak(leesscript, TextToSpeech.QUEUE_FLUSH,null);
    }
}

