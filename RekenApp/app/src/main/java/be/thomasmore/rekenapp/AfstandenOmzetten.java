package be.thomasmore.rekenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class AfstandenOmzetten extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    List<String> meeteenheden = new ArrayList<>();
    List<String> berekeningen = new ArrayList<>();

    EditText afstandText;
    Button bereken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afstanden_omzetten);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        berekeningen= Berekeningen.haalRekengeschiedenisOp(getApplicationContext());
        setupSpinnerMaatheenheid();


        //quickfill
        bereken = (Button)findViewById(R.id.btn_afstanden_omzetten);
        afstandText = (EditText)findViewById(R.id.txt_afstanden_hoeveelheid);

        afstandText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    bereken.requestFocus();
                    omzetten();
                }
                return false;
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void vulMeeteenheden(){
        meeteenheden.add("mm");
        meeteenheden.add("cm");
        meeteenheden.add("dm");
        meeteenheden.add("m");
        meeteenheden.add("km");
    }

    private void setupSpinnerMaatheenheid(){
        vulMeeteenheden();
        useStandardAdapter();
    }

    private void useStandardAdapter(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,meeteenheden);

        final Spinner spinnerMeeteenheden = (Spinner) findViewById(R.id.spinner_afstanden_maateenheid);
        spinnerMeeteenheden.setAdapter(adapter);

    }

    public void onClickButtonOmzettenAfstanden(View V){
        Log.i("dysk","onClickButtonOmzettenAfstanden");
        omzetten();
    }

    private void omzetten(){
         afstandText = (EditText)findViewById(R.id.txt_afstanden_hoeveelheid);
        Spinner maateenhedenSpinner = (Spinner)findViewById(R.id.spinner_afstanden_maateenheid);

        //INPUT CONTROLE
        if (!(afstandText.getText().toString().matches(""))){

            double afstand =Double.parseDouble(afstandText.getText().toString());
            String maateenheid =maateenhedenSpinner.getSelectedItem().toString();

            switch(maateenheid){
                case "mm":
                    afstand = afstand/1000;
                    berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Milimeter omzetten: 1 milimeter = 0.1cm/0.01dm/0.001m/0.000001km",getApplicationContext());
                    break;
                case "cm" :
                    afstand = afstand/100;
                    berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Centimeter omzetten: 1 centimeter = 10mm/0.1dm/0.01m/0.00001km",getApplicationContext());

                    break;
                case "dm":
                    afstand = afstand/10;
                    berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Decimeter omzetten: 1 decimeter = 100mm/10cm/0.1m/0.0001km",getApplicationContext());
                    break;
                case "m":
                    berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Meter omzetten: 1 meter = 1000mm/100cm/10dm/0.001km",getApplicationContext());

                    break;
                case "km":
                    berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Kilometer omzetten: 1 kilometer= 1000000mm/100000cm/10000dm/1000m",getApplicationContext());

                    afstand = afstand*1000;
                    break;
            }

            List afstanden = Berekeningen.aftstandOmzetten(afstand);

            NumberFormat formatter = new DecimalFormat("###.#######");
            String mm = formatter.format(afstanden.get(0));
            String cm = formatter.format(afstanden.get(1));
            String dm = formatter.format(afstanden.get(2));
            String m = formatter.format(afstanden.get(3));
            String km = formatter.format(afstanden.get(4));

            EditText mmText=(EditText)findViewById(R.id.txt_afstanden_output_mm);
            mmText.setText(mm);

            EditText cmText=(EditText)findViewById(R.id.txt_afstanden_output_cm);
            cmText.setText(cm);

            EditText dmText=(EditText)findViewById(R.id.txt_afstanden_output_dm);
            dmText.setText(dm);

            EditText mText=(EditText)findViewById(R.id.txt_afstanden_output_m);
            mText.setText(m);

            EditText kmText=(EditText)findViewById(R.id.txt_afstanden_output_km);
            kmText.setText(km);

            berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"meter omzetten: " + m + "m= "+mm+"mm/"+cm+"cm/"+dm+"dm/"+km+"km/",getApplicationContext());

        }else {
            Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
        }

    }
    /*


    private void toon(String tekst)
    {
        Toast.makeText(getBaseContext(), tekst, Toast.LENGTH_SHORT).show();
    }

*/
}
