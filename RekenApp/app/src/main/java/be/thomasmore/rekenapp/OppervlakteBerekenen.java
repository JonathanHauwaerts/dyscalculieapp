package be.thomasmore.rekenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class OppervlakteBerekenen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    List<String> figuren = new ArrayList<>();
    List<String> berekeningen = new ArrayList<>();
    List<String> meeteenheden = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oppervlakte_berekenen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        berekeningen= Berekeningen.haalRekengeschiedenisOp(getApplicationContext());
        setupSpinnerFiguren();
        setupSpinnerMaatheenheid();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void vulMeeteenheden(){
        figuren.add("Vierkant");
        figuren.add("Rechthoek");
        figuren.add("Driehoek");
        figuren.add("Parallellogram");
        figuren.add("Ruit");
        figuren.add("Trapezium");
        figuren.add("Cirkel");
    }

    private void setupSpinnerFiguren(){
        vulMeeteenheden();
        useStandardAdapter();
    }

    private void useStandardAdapter(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,figuren);

        final Spinner spinnerfiguren = (Spinner) findViewById(R.id.spinner_figuurkeuze);
        spinnerfiguren.setAdapter(adapter);

        spinnerfiguren.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View spinner_figuurkeuze, int position, long id) {
                String figuur = spinnerfiguren.getSelectedItem().toString();
                TextView label1 = (TextView) findViewById(R.id.label_figuur_hoogte);
                EditText text1 = (EditText) findViewById(R.id.txt_figuur_hoogte);

                TextView label2 = (TextView) findViewById(R.id.label_figuur_breedte);
                EditText text2 = (EditText)findViewById(R.id.txt_figuur_breedte);

                TextView label3 = (TextView) findViewById(R.id.label_figuur_extra);
                EditText text3 = (EditText) findViewById(R.id.txt_figuur_extra);


                switch (figuur){
                    case "Vierkant":
                        label1.setText("Lengte: ");

                        label2.setEnabled(false);
                        label2.setVisibility(View.INVISIBLE);
                        label2.setText("Breedte: ");

                        text2.setEnabled(false);
                        text2.setVisibility(View.INVISIBLE);

                        label3.setEnabled(false);
                        label3.setVisibility(View.INVISIBLE);

                        text3.setEnabled(false);
                        text3.setVisibility(View.INVISIBLE);
                        break;
                    case "Rechthoek":
                        label1.setText("Lengte: ");

                        label2.setEnabled(true);
                        label2.setVisibility(View.VISIBLE);
                        label2.setText("Breedte: ");

                        text2.setEnabled(true);
                        text2.setVisibility(View.VISIBLE);

                        label3.setEnabled(false);
                        label3.setVisibility(View.INVISIBLE);

                        text3.setEnabled(false);
                        text3.setVisibility(View.INVISIBLE);
                        break;
                    case "Driehoek":
                        label1.setText("Hoogte: ");

                        label2.setEnabled(true);
                        label2.setVisibility(View.VISIBLE);
                        label2.setText("Breedte: ");

                        text1.setEnabled(true);
                        text1.setVisibility(View.VISIBLE);

                        text2.setEnabled(true);
                        text2.setVisibility(View.VISIBLE);

                        label3.setEnabled(false);
                        label3.setVisibility(View.INVISIBLE);

                        text3.setEnabled(false);
                        text3.setVisibility(View.INVISIBLE);
                        break;
                    case "Parallellogram":
                        label1.setText("Hoogte: ");

                        label2.setEnabled(true);
                        label2.setVisibility(View.VISIBLE);
                        label2.setText("Breedte: ");

                        text1.setEnabled(true);
                        text1.setVisibility(View.VISIBLE);

                        text2.setEnabled(true);
                        text2.setVisibility(View.VISIBLE);

                        label3.setEnabled(false);
                        label3.setVisibility(View.INVISIBLE);

                        text3.setEnabled(false);
                        text3.setVisibility(View.INVISIBLE);
                        break;
                    case "Ruit":
                        label1.setText("Grote diagonaal: ");

                        label2.setEnabled(true);
                        label2.setVisibility(View.VISIBLE);
                        label2.setText("Kleine diagonaal: ");

                        text1.setEnabled(true);
                        text1.setVisibility(View.VISIBLE);

                        text2.setEnabled(true);
                        text2.setVisibility(View.VISIBLE);

                        label3.setEnabled(false);
                        label3.setVisibility(View.INVISIBLE);

                        text3.setEnabled(false);
                        text3.setVisibility(View.INVISIBLE);
                        break;
                    case "Trapezium":
                        label1.setText("Grote basis: ");

                        label2.setEnabled(true);
                        label2.setVisibility(View.VISIBLE);
                        label2.setText("Kleine basis: ");

                        text2.setEnabled(true);
                        text2.setVisibility(View.VISIBLE);

                        label3.setEnabled(true);
                        label3.setVisibility(View.VISIBLE);
                        label3.setText("Hoogte:");

                        text3.setEnabled(true);
                        text3.setVisibility(View.VISIBLE);
                        break;
                    case "cirkel":
                        label1.setText("Straal: ");

                        label2.setEnabled(false);
                        label2.setVisibility(View.INVISIBLE);

                        text2.setEnabled(false);
                        text2.setVisibility(View.INVISIBLE);

                        label3.setEnabled(false);
                        label3.setVisibility(View.INVISIBLE);

                        text3.setEnabled(false);
                        text3.setVisibility(View.INVISIBLE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    private void vulMeeteenheden2(){
        meeteenheden.add("cm");
        meeteenheden.add("dm");
        meeteenheden.add("m");
        meeteenheden.add("km");
    }

    private void setupSpinnerMaatheenheid(){
        vulMeeteenheden2();
        useStandardAdapter2();
    }

    private void useStandardAdapter2(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,meeteenheden);

        final Spinner spinnerMeeteenheden = (Spinner) findViewById(R.id.spinner_meeteenheid);
        spinnerMeeteenheden.setAdapter(adapter);

    }

    public void Berekenen(View v){
        Spinner figuur = (Spinner) findViewById(R.id.spinner_figuurkeuze);
        String figuurTest = figuur.getSelectedItem().toString();
        double hoogte1 = 0.0,breedte1=0.0,extra1=0.0,hoogte=0.0,breedte=0.0,extra=0.0;

        Spinner meeteenheid = (Spinner) findViewById(R.id.spinner_meeteenheid);
        String meeteenheidselected = meeteenheid.getSelectedItem().toString();

        EditText text1 = (EditText) findViewById(R.id.txt_figuur_hoogte);
        if(text1.isEnabled() && !(text1.getText().toString().matches(""))){
        hoogte1 = Double.parseDouble(text1.getText().toString());}
        if(text1.isEnabled()&& text1.getText().toString().matches("")){
            Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
        }

        EditText text2 = (EditText)findViewById(R.id.txt_figuur_breedte);
        if(text2.isEnabled() && !(text2.getText().toString().matches(""))){
        breedte1 = Double.parseDouble(text2.getText().toString());}
        if(text2.isEnabled()&& text2.getText().toString().matches("")){
            Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
        }

        EditText text3 = (EditText) findViewById(R.id.txt_figuur_extra);
        if(text3.isEnabled() && !(text3.getText().toString().matches(""))){
        extra1 = Double.parseDouble(text3.getText().toString());}
        if(text3.isEnabled()&& text3.getText().toString().matches("")){
            Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
        }

        String maat = "m";

        switch (meeteenheidselected){
            case"cm":
                maat = "cm";
                if(hoogte1 != 0.0){
                    hoogte = hoogte1 /100;
                }
                if(breedte1 != 0.0){
                    breedte = breedte1 /100;
                }
                if(extra1 != 0.0){
                    extra = extra1/100;
                }
                break;
            case"dm":
                maat = "dm";
                if(hoogte1 != 0.0){
                    hoogte = hoogte1 / 10;
                }
                if(breedte1 != 0.0){
                    breedte = breedte1 /10;
                }
                if(extra1 != 0.0){
                    extra = extra1/10;
                }
                break;
            case"m":
                maat = "m";
                if(hoogte1 != 0.0){
                    hoogte = hoogte1 ;
                }
                if(breedte1 != 0.0){
                    breedte = breedte1;
                }
                if(extra1 != 0.0){
                    extra = extra1;
                }
                break;
            case"km":
                maat = "km";
                if(hoogte1 != 0.0){
                    hoogte = hoogte1 * 1000;
                }
                if(breedte1 != 0.0){
                    breedte = breedte1 *1000;
                }
                if(extra1 != 0.0){
                    extra = extra1*1000;
                }
                break;
        }

        EditText textCm = (EditText)findViewById(R.id.txt_gewicht_output_cm);
        EditText textDm = (EditText)findViewById(R.id.txt_gewicht_output_dm);
        EditText textm = (EditText)findViewById(R.id.txt_gewicht_output_m);
        EditText textkm = (EditText)findViewById(R.id.txt_gewicht_output_km);
        EditText textAre = (EditText) findViewById(R.id.txt_gewicht_output_are);
        EditText textHectare = (EditText) findViewById(R.id.txt_gewicht_output_hectare);

       List<Double> uitkomst = new ArrayList<>();
        double oppervlakte =0.000000,oppervlakte1=0.0;

        switch (figuurTest){
            case "Vierkant":
                oppervlakte = Berekeningen.oppervlakteRechthoek(hoogte,hoogte);
                oppervlakte1 = Berekeningen.oppervlakteRechthoek(hoogte1,hoogte1);
                uitkomst = Berekeningen.oppervlakteConverter(oppervlakte);
                berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Oppervlakte van een vierkant: Zijde X zijde : " + hoogte1 + maat +" X " + hoogte1 + maat +" ="+oppervlakte1 +maat,getApplicationContext());
                break;
            case "Rechthoek":
                oppervlakte = Berekeningen.oppervlakteRechthoek(hoogte,breedte);
                oppervlakte1 = Berekeningen.oppervlakteRechthoek(hoogte1,breedte1);
                uitkomst = Berekeningen.oppervlakteConverter(oppervlakte);
                berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Oppervlakte van een rechthoek: breedte X hoogte : " + hoogte1 +maat +" X " + breedte1+maat +" ="+oppervlakte1+maat,getApplicationContext());
                break;
            case "Driehoek" :
                oppervlakte = Berekeningen.oppervlakteDriehoek(hoogte,breedte);
                oppervlakte1 = Berekeningen.oppervlakteDriehoek(hoogte1,breedte1);
                uitkomst = Berekeningen.oppervlakteConverter(oppervlakte);
                berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Oppervlakte van een driehoek: (Breedte X hoogte)/2 : (" + hoogte1+maat +" X " + breedte1 +maat+")/2 ="+oppervlakte1+maat,getApplicationContext());
                break;
            case "Parallellogram":
                oppervlakte =Berekeningen.oppervlakteParallellogram(hoogte,breedte);
                oppervlakte1 = Berekeningen.oppervlakteParallellogram(hoogte1,breedte1);
                uitkomst = Berekeningen.oppervlakteConverter(oppervlakte);
                berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Oppervlakte van een parallellogram: Rechtezijde X hoogte: " + hoogte1+maat +" X " + breedte1+maat +" ="+oppervlakte1+maat,getApplicationContext());
                break;
            case "Ruit":
                oppervlakte = Berekeningen.oppervlakteRuit(hoogte,breedte);
                oppervlakte1 = Berekeningen.oppervlakteRuit(hoogte1,breedte1);
                uitkomst = Berekeningen.oppervlakteConverter(oppervlakte);
                berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Oppervlakte van een ruit: Grotediagonaal X kleinediagonaal: " + hoogte1+maat +" X " + breedte1+maat +"="+oppervlakte1+maat,getApplicationContext());
                break;
            case "Trapezium":
                oppervlakte = Berekeningen.oppervlakteTrapezium(hoogte,breedte,extra);
                oppervlakte1 = Berekeningen.oppervlakteTrapezium(hoogte1,breedte1,extra1);
                uitkomst = Berekeningen.oppervlakteConverter(oppervlakte);
                berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Oppervlakte van een trapezium: (grotebasis + kleinebasis) X hoogte / 2: (" + hoogte1+maat +" + " + breedte1+maat +") X "+extra1+maat + "/ 2 ="+oppervlakte1+maat,getApplicationContext());
                break;
            case "cirkel":
                oppervlakte = Berekeningen.oppervlakteCirkel(hoogte);
                oppervlakte1 = Berekeningen.oppervlakteCirkel(hoogte1);
                uitkomst = Berekeningen.oppervlakteConverter(oppervlakte);
                berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Oppervlakte van een cirkel: (straal X straal) X pi : (" + hoogte1+maat +" X " + hoogte1+maat +") X 3.14="+oppervlakte1+maat,getApplicationContext());
                break;
        }
        NumberFormat formatter = new DecimalFormat("###.#########");
        String mm = formatter.format(uitkomst.get(0));
        String cm = formatter.format(uitkomst.get(1));
        String m = formatter.format(uitkomst.get(2));
        String km = formatter.format(uitkomst.get(3));
        String are = formatter.format(uitkomst.get(4));
        String hectare = formatter.format(uitkomst.get(5));
        berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Omzetten vierkante meter: 1m² = 1000000mm²/10000cm²/0.000001km²",getApplicationContext());
        berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Omzetten are: 1are=100m²/0.01hectare",getApplicationContext());

        textCm.setText(mm);
        textDm.setText(cm);
        textm.setText(m);
        textkm.setText(km);

        textAre.setText(are);
        textHectare.setText(hectare);
    }
}
