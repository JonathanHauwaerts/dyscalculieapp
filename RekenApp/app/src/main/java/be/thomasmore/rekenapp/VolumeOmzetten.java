package be.thomasmore.rekenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class VolumeOmzetten extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    List<String> meeteenheden = new ArrayList<>();
    List<String> berekeningen = new ArrayList<>();

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";

    //quickfill
    EditText volumeText;
    Button bereken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume_omzetten);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        berekeningen= Berekeningen.haalRekengeschiedenisOp(getApplicationContext());
        setupSpinnerMaatheenheid();

        //quickfill
        volumeText = (EditText)findViewById(R.id.txt_volume_hoeveelheid);
        bereken = (Button)findViewById(R.id.btn_volume_berekenen);

        volumeText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    bereken.requestFocus();
                    omzettenVolume();
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void vulMeeteenheden(){
        meeteenheden.add("ml");
        meeteenheden.add("cl");
        meeteenheden.add("dl");
        meeteenheden.add("l");
    }

    private void setupSpinnerMaatheenheid(){
        vulMeeteenheden();
        useStandardAdapter();
    }

    private void useStandardAdapter(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,meeteenheden);

        final Spinner spinnerMeeteenheden = (Spinner) findViewById(R.id.spinner_volume_maateenheid);
        spinnerMeeteenheden.setAdapter(adapter);

    }

    public void onClickButtonOmzettenVolume(View V){
        Log.i("dysk","onclickbuttonomzettenvolume");
        omzettenVolume();
    }

    private void omzettenVolume(){

        Spinner maateenheidSpinner = (Spinner)findViewById(R.id.spinner_volume_maateenheid);

        if (!(volumeText.getText().toString().matches(""))){
            double volume = Double.parseDouble(volumeText.getText().toString());
            String maateenheid = maateenheidSpinner.getSelectedItem().toString();

            switch(maateenheid){
                case "ml":
                    volume = volume/10;
                    break;
                case "cl" :
                    break;
                case "dl":
                    volume = volume*10;
                    break;
                case "l":
                    volume = volume*100;
                    break;
            }



            List volumes = Berekeningen.omzettenVolume(volume);

            NumberFormat formatter = new DecimalFormat("###.#######");
            String ml = formatter.format(volumes.get(0));
            String cl = formatter.format(volumes.get(1));
            String dl = formatter.format(volumes.get(2));
            String l = formatter.format(volumes.get(3));

            EditText mlText=(EditText)findViewById(R.id.txt_volume_output_ml);
            mlText.setText(ml);

            EditText clText=(EditText)findViewById(R.id.txt_volume_output_cl);
            clText.setText(cl);

            EditText dlText=(EditText)findViewById(R.id.txt_volume_output_dl);
            dlText.setText(dl);

            EditText lText=(EditText)findViewById(R.id.txt_volume_output_l);
            lText.setText(l);

            berekeningen = Berekeningen.vulRekengeschiedenis(berekeningen,"Volume omzetten: "+cl+"cl=" + ml+"ml/"+dl+"dl/"+l+"l",getApplicationContext());


        }else {
            Toast.makeText(getBaseContext(), "Gelieve alle velden boven de knop in te vullen.", Toast.LENGTH_SHORT).show();
        }
       }




    private void toon(String tekst)
    {
        Toast.makeText(getBaseContext(), tekst, Toast.LENGTH_SHORT).show();
    }


}
