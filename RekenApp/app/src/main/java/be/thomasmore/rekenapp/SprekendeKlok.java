package be.thomasmore.rekenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AnalogClock;
import android.widget.DigitalClock;
import android.widget.TimePicker;

import org.joda.time.LocalDateTime;

import java.util.Locale;

public class SprekendeKlok extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    AnalogClock analogeklok;
    DigitalClock digitaleklok;
    TimePicker tijdpicker;
    TextToSpeech spreker;
    LocalDateTime tijdpickerUur;

    private static final String PREFS_NAME = "prefs";
    private static final String PREF_DARK_THEME = "dark_theme";
    private static final String PREF_SPREKENDE_TIMEPICKER = "pratende_tijdpicker";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean useDarkTheme = preferences.getBoolean(PREF_DARK_THEME, false);
        final boolean tijdpickerPraat = preferences.getBoolean(PREF_SPREKENDE_TIMEPICKER,false);
        if(useDarkTheme) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sprekende_klok);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tijdpickerUur = new LocalDateTime().now();

        spreker = new TextToSpeech(getApplicationContext(),new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                spreker.setLanguage(new Locale("nl_BE"));
            }
        });
        //TODO change this language

        tijdpicker = (TimePicker) findViewById(R.id.TimePicker_sprekende_klok);
        tijdpicker.setIs24HourView(true);


        tijdpicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                //CHANGE BACK TO HOUR & MINUTE
                LocalDateTime nu = new LocalDateTime(2017,12,12,hour,minute);
                tijdpickerUur = nu;
                if (tijdpickerPraat){
                    zegTijd(nu);
                }

            }
        });

        analogeklok = (AnalogClock)findViewById(R.id.analogClock_sprekende_klok);
        analogeklok.setOnClickListener(analogeklokOnClickListener);

        digitaleklok = (DigitalClock)findViewById(R.id.digitalClock_sprekende_klok);
        digitaleklok.setOnClickListener(digitaleklokOnClickListener);

    }

    private void zegTijd(LocalDateTime nu){
        String leesscript;
        leesscript = "Het is " + String.valueOf(nu.getHourOfDay()) + " uur en " + String.valueOf(nu.getMinuteOfHour())+ " minuten";
        //  TODO OPTIONEEL ALS ER TIJD OVER IS
        int minuten = nu.getMinuteOfHour();
        int uur = nu.getHourOfDay();
        boolean isLocked = false;

        if (minuten == 50){
            isLocked = true;
            //het is half uur+1
            if (uur <=12){
                uur = uur+1;
                leesscript ="Het is tien voor " + uur;
            }else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is tien voor " + uur;
                }
            }
        }
        if (minuten == 10){
            isLocked = true;
            //het is half uur+1
            //if (uur <=12){

                leesscript ="Het is tien over " + uur;
           /* } else{
                uur = (uur-12);
                uur = Math.abs(uur);

                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is tien over " + uur;
                }
            }*/
        }
        if (minuten == 30){
            isLocked = true;
            //het is half uur+1
            //if (uur <=12){
                uur = uur+1;
                leesscript ="Het is half " + uur;
            /*}else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is half " + uur;
                }
            }*/
        }
        if (minuten == 15){
            isLocked = true;
           // if (uur <=12){

                leesscript ="Het is kwart over " + uur;
         //   }else{
                uur = (uur-12);
                uur = Math.abs(uur);

             /*   if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is kwart over " + uur;
                }
            }*/
        }
        if (minuten==45){
            isLocked = true;
            //het is kwart voor uur+1
           // if (uur <=12){
                uur = uur+1;
                leesscript ="Het is kwart voor " + uur;
           /* }else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is kwart voor " + uur;
                }
            }*/
        }
        if (nu.getMinuteOfHour()==0){
            //het is uur
            //het is half uur+1
            isLocked = true;
           // if (uur <=12){
                uur = uur+1;
                leesscript ="Het is " + uur + " uur";
                isLocked = true;
           /* }else{
                uur = (uur-12);
                uur = Math.abs(uur);
                uur = uur +1;
                if (uur >= 0){
                    if (uur == 0){
                        uur = 12;
                    }
                    leesscript = "Het is  " + uur + " uur";
                }
                isLocked = true;
            }*/
        }else {
            //het is half uur+1
            if (isLocked == false){
                //if (uur <=12){

                    leesscript ="Het is  " + minuten + " over " + uur ;
                    if (minuten >=30){
                        uur = uur+1;
                        minuten = 60 - minuten;
                        leesscript = "Het is " + minuten + " voor " +  uur ;
                //    }

               /* }else{
                    uur = (uur-12);
                    uur = Math.abs(uur);

                    if (uur >= 0){
                        if (uur == 0){
                            uur = 12;
                        }
                        leesscript = "Het is  " + uur + " uur " + minuten;
                        if (minuten >=30){
                            uur = uur+1;
                            minuten = 60 - minuten;
                            leesscript = "Het is " + minuten + " voor " +  uur;
                        }
                    }*/
                }
            }

        }


        spreker.speak(leesscript, TextToSpeech.QUEUE_FLUSH,null);
    }

    private AnalogClock.OnClickListener analogeklokOnClickListener = new AnalogClock.OnClickListener(){
        @Override
        public void onClick(View v){
            LocalDateTime nu = new LocalDateTime().now();

            zegTijd(nu);

        }
    };

    private DigitalClock.OnClickListener digitaleklokOnClickListener = new DigitalClock.OnClickListener(){
        @Override
        public void onClick(View v){
            LocalDateTime nu = new LocalDateTime().now();

            zegTijd(nu);

        }
    };

    public void onClickButtonSprekendeKlok_huidigUur(View v){
        LocalDateTime nu = new LocalDateTime().now();
        zegTijd(nu);
    }

    public void onClickButtonSprekendeKlok_TimePickerPraat(View v){
        zegTijd(tijdpickerUur);
    }



    protected void  onDestroy(){
        super.onDestroy();
        spreker.shutdown();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Instellingen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.action_about) {
            Intent intent= new Intent(this, About.class);
            startActivity(intent);
            finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_solden) {
            Intent intent= new Intent(this, SoldenBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_wisselgeld) {
            Intent intent= new Intent(this, WisselgeldBerekenen.class);
            startActivity(intent);
            finish();
            return true;

        } else if (id == R.id.nav_geldtellen) {
            Intent intent= new Intent(this, GeldTellen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijd) {
            Intent intent= new Intent(this, TijdBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_datumsberekenen) {
            Intent intent= new Intent(this, DatumBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_omzetten) {
            Intent intent= new Intent(this, Omzetten.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_oppervlakteberekenen) {
            Intent intent= new Intent(this, OppervlakteBerekenen.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_rekengeschiedenis) {
            Intent intent= new Intent(this, RekenGeschiedenis.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_tijdafstand) {
            Intent intent= new Intent(this, TijdAfstand.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_datumdagenrekenen) {
            Intent intent= new Intent(this, DatumDagenRekenen.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_sprekendeklok) {
            Intent intent= new Intent(this, SprekendeKlok.class);
            startActivity(intent);
            finish();
            return true;
        }else if (id == R.id.nav_geldgeven) {
            Intent intent= new Intent(this, GeldGeven.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



}
